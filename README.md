# readme-samples
[![Build Status](https://travis-ci.org/abel533/Mapper.svg?branch=master)](https://travis-ci.org/abel533/Mapper)
[![Maven central](https://maven-badges.herokuapp.com/maven-central/tk.mybatis/mapper/badge.svg)](https://maven-badges.herokuapp.com/maven-central/tk.mybatis/mapper)

这是一个Readme的示例，在2020的软件公司，开发文档和代码是很重要的一部分，Gitlab不只是一个代码托管工具，`Readme`、`WIKI`、`Issue`、`PR`都是可以被利用起来的。`Readme`代表着项目的整体介绍，`WIKI`代表着项目的接口文档，`Issue`可被追踪的缺陷与优化，`PR`多人协作。  
同时项目中需要引入jar包，给出一个`pom`示例，`Getting started`示例调用。  
同时包含对文档界面的Link。

## Getting started
- 添加Maven仓库
  ```
  <dependency>
    <groupId>com.dcpms.service.biz.flow</groupId>
    <artifactId>dcpms-service-biz-flow</artifactId>
  </dependency>
  ```
- 示例应用
  1. Hystric的定义
   ```
   public class BizFlowHystricFallbackFactory implements FallbackFactory<IBizFlowHystric> {
        @Override
	    public IBizFlowHystric create(Throwable throwable) {
            ...
        }
   }

   @FeignClient(...)
   public interface IBizFlowHystric  {

       /**
       * 新插入业务流程，返回下一节点的状态
       */
       @PostMapping("/insertFlow")
       CommonMsg<FlowBusinessVO> insertFlow(@RequestBody CommonMsg<FlowBusinessVO> commonMsg);

   }
   ```

    2. Feign调用
    ```
    bizFlowHystric.insertFlow(...); // 根绝业务需要保存返回的流程实例ID与流程节点值
    ```

## Link
- [文档](doc/API.md)

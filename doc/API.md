## Contents
- [流程新增/审批    `POST /bizFlow/insertFlow`](api/bizflow.md)
> ~~简单介绍:~~ 流程引擎新增/审批接口，如果是第一次则新增，后续为审批，通过处理结果值区分不同的审批类型, `900001`转派, `900002`委托, `900003`暂停, `900004`唤醒, 其余为审批处理
- [流程新增/审批    `POST /bizFlow/insertFlow`](api/bizflow.md)
- [工作台列表   `GET /bizFlow/workbench`](api/bizflow.md)
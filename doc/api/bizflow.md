## BizFlowController
- 新增/审核流程  
> 地址：`POST /bizFlow/insertFlow`  
> 请求数据(**application/json**):   
> ```
> {
>  "head": {
>    "transSeqNo": null,
>    "chainPath": null,
>    "chainnelId": null,
>    "transCode": null,
>    "transTime": null,
>    "respTime": null,
>    "respCode": "0000",
>    "errorCode": "0000",
>    "errorMsg": {
>      
>    },
>    "token": null,
>    "projectNo": null
>  },
>  "body": {
>    "listBody": [
>      {
>        "tenantsNo": "BJSY",
>        "bizProcesTypId": "ce-ywxq",
>        "bizDataInternlId": "7",
>        "apprvDealWthStageCd": "11_XQ_TC",
>        "apprvDealWthResultCd": "100000",
>        "apprvDealWthTm": "",
>        "apprvDealWthPersonId": "1",
>        "apprvDealWthSugstn": "",
>        "flowableProcessParam": {
>          "deptLevelCd": "1",
>          "kjkDeptId": "kjk",
>          "bizReqrmntTypCd": "2",
>          "hcIdentity": "1",
>          "BARoleId": 1,
>          "SARoleId": 2,
>          "ywcDeptId": "ywc",
>          "PMUserId": "1440"
>        }
>      }
>    ],
>    "pageNum": 0,
>   "pageSize": 0,
>    "total": 0
>  }
>}
> ```
> 返回(**application/json**):  
>```
>{
>  "head": {
>    "transSeqNo": null,
>    "chainPath": null,
>    "chainnelId": null,
>    "transCode": null,
>    "transTime": null,
>    "respTime": null,
>    "respCode": "0000",
>    "errorCode": "0000",
>    "errorMsg": {},
>    "token": null,
>    "projectNo": null
>  },
>  "body": {
>    "listBody": [
>      {
>        "validFlg": null,
>        "createrId": null,
>        "createrName": null,
>        "crtTm": null,
>        "modifierId": null,
>        "modifierName": null,
>        "modfyTm": null,
>        "remrkField1": null,
>        "remrkField2": null,
>        "remrkField3": null,
>        "remrkField4": null,
>        "remrkField5": null,
>        "tenantsNo": null,
>        "apprvProcesKepRecdInternlId": null,
>        "bizProcesTypId": null,
>        "apprvProcesExmplId": "34e6cbb7-cfbd-11e9-8c97-02424eceeb28",
>        "apprvProcesNodeId": "11_XQ_TC",
>        "bizDataInternlId": null,
>        "apprvDealWthStageCd": "161_XQ_SL",
>        "apprvDealWthResultCd": null,
>        "apprvDealWthDeptId": null,
>        "apprvDealWthDeptIdName": null,
>        "apprvDealWthPersonId": null,
>        "apprvDealWthPersonIdName": null,
>        "apprvDealWthSugstn": null,
>        "apprvDealWthTm": null,
>        "apprvHours": 0,
>        "apprvProcesWarnTm": null,
>        "apprvProcesUpAscTm": null,
>        "nextOneDealWthPersonIdAssmblg": null,
>        "nextOneDealWthPersonIdAssmblgName": null,
>        "processEndFlg": false,
>        "flowableProcessParam": null,
>        "taskId": "34f68347-cfbd-11e9-8c97-02424eceeb28"
>      }
>    ],
>    "pageNum": 0,
>    "pageSize": 0,
>    "total": 0
>  }
>}
>```  

- 新增/审核流程  
> 地址：`POST /bizFlow/insertFlow`  
> 请求数据(**application/json**):   
> ```
> {
>  "head": {
>    "transSeqNo": null,
>    "chainPath": null,
>    "chainnelId": null,
>    "transCode": null,
>    "transTime": null,
>    "respTime": null,
>    "respCode": "0000",
>    "errorCode": "0000",
>    "errorMsg": {
>      
>    },
>    "token": null,
>    "projectNo": null
>  },
>  "body": {
>    "listBody": [
>      {
>        "tenantsNo": "BJSY",
>        "bizProcesTypId": "ce-ywxq",
>        "bizDataInternlId": "7",
>        "apprvDealWthStageCd": "11_XQ_TC",
>        "apprvDealWthResultCd": "100000",
>        "apprvDealWthTm": "",
>        "apprvDealWthPersonId": "1",
>        "apprvDealWthSugstn": "",
>        "flowableProcessParam": {
>          "deptLevelCd": "1",
>          "kjkDeptId": "kjk",
>          "bizReqrmntTypCd": "2",
>          "hcIdentity": "1",
>          "BARoleId": 1,
>          "SARoleId": 2,
>          "ywcDeptId": "ywc",
>          "PMUserId": "1440"
>        }
>      }
>    ],
>    "pageNum": 0,
>   "pageSize": 0,
>    "total": 0
>  }
>}
> ```
> 返回(**application/json**):  
>```
>{
>  "head": {
>    "transSeqNo": null,
>    "chainPath": null,
>    "chainnelId": null,
>    "transCode": null,
>    "transTime": null,
>    "respTime": null,
>    "respCode": "0000",
>    "errorCode": "0000",
>    "errorMsg": {},
>    "token": null,
>    "projectNo": null
>  },
>  "body": {
>    "listBody": [
>      {
>        "validFlg": null,
>        "createrId": null,
>        "createrName": null,
>        "crtTm": null,
>        "modifierId": null,
>        "modifierName": null,
>        "modfyTm": null,
>        "remrkField1": null,
>        "remrkField2": null,
>        "remrkField3": null,
>        "remrkField4": null,
>        "remrkField5": null,
>        "tenantsNo": null,
>        "apprvProcesKepRecdInternlId": null,
>        "bizProcesTypId": null,
>        "apprvProcesExmplId": "34e6cbb7-cfbd-11e9-8c97-02424eceeb28",
>        "apprvProcesNodeId": "11_XQ_TC",
>        "bizDataInternlId": null,
>        "apprvDealWthStageCd": "161_XQ_SL",
>        "apprvDealWthResultCd": null,
>        "apprvDealWthDeptId": null,
>        "apprvDealWthDeptIdName": null,
>        "apprvDealWthPersonId": null,
>        "apprvDealWthPersonIdName": null,
>        "apprvDealWthSugstn": null,
>        "apprvDealWthTm": null,
>        "apprvHours": 0,
>        "apprvProcesWarnTm": null,
>        "apprvProcesUpAscTm": null,
>        "nextOneDealWthPersonIdAssmblg": null,
>        "nextOneDealWthPersonIdAssmblgName": null,
>        "processEndFlg": false,
>        "flowableProcessParam": null,
>        "taskId": "34f68347-cfbd-11e9-8c97-02424eceeb28"
>      }
>    ],
>    "pageNum": 0,
>    "pageSize": 0,
>    "total": 0
>  }
>}
>```